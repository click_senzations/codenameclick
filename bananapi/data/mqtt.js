var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://test.mosquitto.org');
var database;
var socket;

module.exports.connect = function (db, soc) {
    database = db;
    socket = soc;
    client.on('connect', function () {
        console.log('Connected');
        client.subscribe('click_senzations/#');
    });
}

var extractDevice = function (topic) {
    return topic.replace("click_senzations/", "");
}

client.on('message', function (topic, message) {
    try {
        console.log(message);
        console.log(topic);
        if (message != '') {
            socket.emit('mqtt', {'topic': topic, 'message': message.toString()})
            var pure_topic = extractDevice(topic)
            database.insertNewRecord(pure_topic, message.toString());
            if (pure_topic == 'relay_board') {

            }
        }

    }
    catch (err) {
        console.log("Error processing MQTT message:" + message + " Error is" + err);
    }
});

module.exports.getSocket = function () {
    return client;
}

module.exports.relayEvent = function (data) {
    console.log("Trying to publish");
    client.publish("click_senzations/relayEvent", data, {'qos':1,'retain':true}, function () {
        console.log("Message is published");
    });
}
