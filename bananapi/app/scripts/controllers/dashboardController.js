'use strict';
/**
 * @ngdoc function
 * @name smartBuildingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the smartBuildingApp
 */
angular.module('smartBuildingApp')
    .controller('DashboardCtrl', ['$scope', '$timeout', 'mqttProvider' , function ($scope, $timeout, mqttProvider) {
        $scope.temp1 = '-'
        $scope.humidity1 = '-'
        $scope.awesomes = {
            data: [
                {label: "Healthiness", value: 78, color: "#d62728", suffix: "%"}
            ],
            options: {thickness: 10, mode: "gauge", total: 100}
        }
        $scope.events = [
        ]
        mqttProvider.on('mqtt', function (data) {
            console.log(data);
            if (data.topic == 'click_senzations/temp1') {
                $scope.temp1 = parseInt(data.message, 10) + ' °C';
            }
            if (data.topic == 'click_senzations/humidity1') {
                $scope.humidity1 = parseInt(data.message, 10) + ' %RH';
            }
            if (data.topic == 'click_senzations/light1') {
                $scope.light1 = parseInt(data.message, 10) + ' %';
            }
            if (data.topic == 'click_senzations/awesomes') {
                $scope.awesomes.data[0].value = parseFloat(data.message).toFixed(2);
            }
            if (data.topic == 'click_senzations/sensors') {
                $scope.sensors = JSON.parse(data.message);
            }
            if (data.topic == 'click_senzations/too_low_light') {
                $scope.events.push({
                    headline: "It is too dark !!!",
                    timestamp: (new Date()).toLocaleTimeString(),
                    icon: 'fa-sun-o',
                    comments: "Suggest to go outside or turn on more lights."
                })
            }
            if (data.topic == 'click_senzations/too_hot') {
                $scope.events.push({
                    headline: "It is too hot !!!",
                    timestamp: (new Date()).toLocaleTimeString(),
                    icon: 'fa-tachometer',
                    comments: "Cooling system has been initialized."
                })
            }
        });


    }]);